<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')->name('api.')->group(function(){

    Route::get('/classifications', 'ClassificationController@index')->name('list_classifications');
    Route::get('/classifications/{id}', 'ClassificationController@show')->name('view_classifications');
    Route::post('/classifications', 'ClassificationController@add')->name('add_classifications');
    Route::put('/classifications/{id}', 'ClassificationController@edit')->name('edit_classifications');
    Route::delete('/classifications/{id}', 'ClassificationController@remove')->name('remove_classifications');

    Route::get('/actors', 'ActorController@index')->name('list_actors');
    Route::get('/actors/{id}', 'ActorController@show')->name('view_actors');
    Route::post('/actors', 'ActorController@add')->name('add_actors');
    Route::put('/actors/{id}', 'ActorController@edit')->name('edit_actors');
    Route::delete('/actors/{id}', 'ActorController@remove')->name('remove_actors');

    Route::get('/directors', 'DirectorController@index')->name('list_directors');
    Route::get('/directors/{id}', 'DirectorController@show')->name('view_directors');
    Route::post('/directors', 'DirectorController@add')->name('add_directors');
    Route::put('/directors/{id}', 'DirectorController@edit')->name('edit_directors');
    Route::delete('/directors/{id}', 'DirectorController@remove')->name('remove_directors');

    Route::get('/movies', 'MoviesController@index')->name('list_movies');
    Route::get('/movies/{id}', 'MoviesController@show')->name('view_movies');
    Route::post('/movies', 'MoviesController@add')->name('add_movies');
    Route::put('/movies/{id}', 'MoviesController@edit')->name('edit_movies');
    Route::delete('/movies/{id}', 'MoviesController@remove')->name('remove_movies');

    Route::get('/movies-actors', 'MoviesActorsController@index')->name('list_movies-actors');
    Route::get('/movies-actors/{id}', 'MoviesActorsController@show')->name('view_movies-actors');
    Route::post('/movies-actors', 'MoviesActorsController@add')->name('add_movies-actors');
    Route::put('/movies-actors/{id}', 'MoviesActorsController@edit')->name('edit_movies-actors');
    Route::delete('/movies-actors/{id}', 'MoviesActorsController@remove')->name('remove_movies-actors');

});
