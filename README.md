## About Locadora API

Está é uma API REST utilizando Laravel para cadastro de:

- Filmes
- Classificação do Filme
- Atores
- Diretor
  
A versão do Laravel utilizada para criação desta API é a `7` e o banco de dados utilizado para armazenamento é o MySQL.

Este projeto possuí uma estrutura de banco de dados simples com as seguintes regras de Negócio: 
- Diretores tem um nome e data de nascimento
- Atores tem um nome e data de nascimento
- Classificação tem um nome
- Filmes tem um nome, ano de lançamento, diretor do filme, classificação do filme e a sinopse
- Um filme pode ser feito por vários atores e vários atores podem fazer um filme.

O projeto contem as seguintes tabelas: `movies`, `actors`, `classifications`, `directors` e `movies_actors`. 

A api contem uma model e um controller para manutenção de cada uma dessas tabelas, a lista de rotas para acessar e efetuar a manutenção dos dados esta listado mais abaixo neste arquivo.

## Como startar a API

1. Renomeie o arquivo `.env.example` para `.env` e adicione as configurações do MySQL da sua máquina. 

2. Digite o commando "composer install". Ele vai instalar todos os pacotes php necessários.

3. Digite o commando "php artisan key:generate". Esse vai gerar uma chave para sua aplicação. Sem isso o Laravel não vai funcionar

4. Agora o próximo passo é para criar o banco de dados no seu MySQL, para isso execute o seguinte comando: `CREATE DATABASE locadora`.

5. O próximo e último passo agora é executar as migrations, para isso rode o comando `php artisan migrate` dentro do **prompt de comando** da sua preferencia.

## Como rodar o projeto

Para rodar o projeto utilize o comando `php artisan serve` no prompt de comando dentro da pasta raiz do projeto.

## Lista de rotas da API 

`GET`    [/classifications]      -- lista todas as classificações cadastradas 

`GET`    [/classifications/{id}] -- retorna a classificação com o ID informado

`POST`   [/classifications]      -- adiciona uma nova classificação por meio de requisição enviando um json no BODY

`PUT`    [/classifications/{id}] -- editar a classificação com o ID informado por meio de requisição enviando um json no BODY

`DELETE` [/classifications/{id}] -- apaga a classificação com o ID informado

`GET`    [/actors]      -- lista todos os atores cadastrados 

`GET`    [/actors/{id}] -- retorna o ator com o ID informado

`POST`   [/actors]      -- adiciona um novo ator por meio de requisição enviando um json no BODY

`PUT`    [/actors/{id}] -- editar o ator com o ID informado por meio de requisição enviando um json no BODY

`DELETE` [/actors/{id}] -- apaga o ator com o ID informado

`GET`    [/directors]      -- lista todos os diretores cadastrados

`GET`    [/directors/{id}] -- retorna o diretor com o ID informado

`POST`   [/directors]      -- adiciona um novo diretor por meio de requisição enviando um json no BODY

`PUT`    [/directors/{id}] -- editar o diretor com o ID informado por meio de requisição enviando um json no BODY

`DELETE` [/directors/{id}] -- apaga o diretor com o ID informado

`GET`    [/movies]      -- lista todos os filmes cadastrados

`GET`    [/movies/{id}] -- retorna o filme com o ID informado

`POST`   [/movies]      -- adiciona um novo filme por meio de requisição enviando um json no BODY

`PUT`    [/movies/{id}] -- editar o filme com o ID informado por meio de requisição enviando um json no BODY

`DELETE` [/movies/{id}] -- apaga o filme com o ID informado

`GET`    [/movies-actors]      -- lista todos os registros entre atores e filmes cadastrados

`GET`    [/movies-actors/{id}] -- retorna o registro entre ator e filme do ID informado

`POST`   [/movies-actors]      -- adiciona um novo registro entre ator e filme por meio de requisição enviando um json no BODY

`PUT`    [/movies-actors/{id}] -- editar o registro com o ID informado por meio de requisição enviando um json no BODY

`DELETE` [/movies-actors/{id}] -- apaga o registro com o ID informado
