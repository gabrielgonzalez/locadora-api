<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    protected $fillable = ['nome_class'];

    public function movies()
    {
        return $this->hasMany('App\Movies');
    }
}
