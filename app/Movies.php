<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    protected $fillable = ['nome_filme', 'ano_lancamento', 'sinopse', 'classifications_id', 'directors_id'];

    public function moviesActors()
    {
        return $this->hasMany('App\MoviesActors');
    }

    function classification() {
        return $this->belongsTo('App\Classification');
    }

    function directors() {
        return $this->belongsTo('App\Director');
    }
}
