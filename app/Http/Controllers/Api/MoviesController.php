<?php

namespace App\Http\Controllers\Api;

use App\Movies;
use App\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MoviesController extends Controller
{
    private $movies;

    public function __construct(Movies $movies)
    {
        $this->movies = $movies;
    }

    public function index()
    {
        $data = ['data' => $this->movies
                                ->join('directors', 'directors.id', '=', 'movies.directors_id')
                                ->join('classifications', 'classifications.id', '=', 'movies.classifications_id')
                                ->select('movies.*', 'directors.nome_diretor', 'classifications.nome_class')
                                ->get()];
        return response()->json($data);
    }

    public function show($id)
    {
        $movies = $this->movies
                        ->with('moviesActors')
                        ->join('directors', 'directors.id', '=', 'movies.directors_id')
                        ->join('classifications', 'classifications.id', '=', 'movies.classifications_id')
                        ->find($id);

        if (! $movies) {
            return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
        }

        $data = ['data' => $movies];
        return response()->json($data);
    }

    public function add(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_filme'      => 'required|string',
                    'ano_lancamento' => 'required|string',
                    'sinopse' => 'required|string',
                    'classifications_id' => 'required|integer',
                    'directors_id' => 'required|integer',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $this->movies->create($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Filme criado com sucesso!', 201), 201);

        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function edit(Request $request, $id)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_filme'      => 'required|string',
                    'ano_lancamento' => 'required|string',
                    'sinopse' => 'required|string',
                    'classifications_id' => 'required|integer',
                    'directors_id' => 'required|integer',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $movies     = $this->movies->find($id);

            if (! $movies) {
                return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
            }

            $movies->update($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Filme atualizada com sucesso!', 200), 400);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function remove($id)
    {
        try {

            $movies     = $this->movies->find($id);

            if (! $movies) {
                return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
            }

            $movies->delete();

            return response()->json(['data' => ['message' => 'Filme: ' . $movies->name . ' removido com sucesso!']], 200);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500));
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }
}
