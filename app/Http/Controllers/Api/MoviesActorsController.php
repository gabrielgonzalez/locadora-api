<?php

namespace App\Http\Controllers\Api;

use App\MoviesActors;
use App\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MoviesActorsController extends Controller
{
    private $moviesActors;

    public function __construct(MoviesActors $moviesActors)
    {
        $this->moviesActors = $moviesActors;
    }

    public function index()
    {
        $data = ['data' => $this->moviesActors
                                ->join('movies', 'movies.id', '=', 'movies_actors.movies_id')
                                ->join('actors', 'actors.id', '=', 'movies_actors.actors_id')
                                ->select('movies_actors.*', 'movies.nome_filme', 'actors.nome_ator')
                                ->get()];
        return response()->json($data);
    }

    public function show($id)
    {
        $moviesActors = $this->moviesActors
                            ->join('movies', 'movies.id', '=', 'movies_actors.movies_id')
                            ->join('actors', 'actors.id', '=', 'movies_actors.actors_id')
                            ->select('movies_actors.*', 'movies.nome_filme', 'actors.nome_ator')
                            ->find($id);

        if (! $moviesActors) {
            return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
        }

        $data = ['data' => $moviesActors];
        return response()->json($data);
    }

    public function add(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'movies_id'      => 'required|integer',
                    'actors_id' => 'required|integer',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $this->moviesActors->create($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Filme criada com sucesso!', 201), 201);

        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function edit(Request $request, $id)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'movies_id'      => 'required|integer',
                    'actors_id' => 'required|integer',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $moviesActors     = $this->moviesActors->find($id);

            if (! $moviesActors) {
                return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
            }

            $moviesActors->update($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Filme atualizada com sucesso!', 200), 400);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function remove($id)
    {
        try {

            $moviesActors     = $this->moviesActors->find($id);

            if (! $moviesActors) {
                return response()->json(\App\API\ApiError::errorMessage('Filme não encontrado!', 404), 404);
            }

            $moviesActors->delete();

            return response()->json(['data' => ['message' => 'Filme: ' . $moviesActors->name . ' removido com sucesso!']], 200);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500));
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }
}
