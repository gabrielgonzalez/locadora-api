<?php

namespace App\Http\Controllers\Api;

use App\Director;
use App\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectorController extends Controller
{
    private $director;

    public function __construct(Director $director)
    {
        $this->director = $director;
    }

    public function index()
    {
        $data = ['data' => $this->director->all()];
        return response()->json($data);
    }

    public function show($id)
    {
        $director = $this->director->find($id);

        if (! $director) {
            return response()->json(\App\API\ApiError::errorMessage('Diretor(a) não encontrado!', 404), 404);
        }

        $data = ['data' => $director];
        return response()->json($data);
    }

    public function add(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_diretor'      => 'required|string',
                    'data_nasc_diretor' => 'required|date',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $this->director->create($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Diretor(a) criada com sucesso!', 201), 201);

        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function edit(Request $request, $id)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_diretor'      => 'required|string',
                    'data_nasc_diretor' => 'required|date',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $director     = $this->director->find($id);

            if (! $director) {
                return response()->json(\App\API\ApiError::errorMessage('Diretor(a) não encontrado!', 404), 404);
            }

            $director->update($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Diretor(a) atualizada com sucesso!', 200), 400);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function remove($id)
    {
        try {

            $director     = $this->director->find($id);

            if (! $director) {
                return response()->json(\App\API\ApiError::errorMessage('Diretor(a) não encontrado!', 404), 404);
            }

            $director->delete();

            return response()->json(['data' => ['message' => 'Diretor(a): ' . $director->name . ' removido com sucesso!']], 200);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500));
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }
}
