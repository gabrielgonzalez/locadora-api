<?php

namespace App\Http\Controllers\Api;

use App\Classification;
use App\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClassificationController extends Controller
{
    private $classification;

    public function __construct(Classification $classification)
    {
        $this->classification = $classification;
    }

    public function index()
    {
        $data = ['data' => $this->classification->all()];
        return response()->json($data);
    }

    public function show($id)
    {
        $classification = $this->classification->find($id);

        if (! $classification) {
            return response()->json(\App\API\ApiError::errorMessage('Classificação não encontrado!', 404), 404);
        }

        $data = ['data' => $classification];
        return response()->json($data);
    }

    public function add(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_class'      => 'required|string',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $this->classification->create($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Classificação criada com sucesso!', 201), 201);

        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function edit(Request $request, $id)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_class'      => 'required|string',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $classification     = $this->classification->find($id);

            if (! $classification) {
                return response()->json(\App\API\ApiError::errorMessage('Classificação não encontrado!', 404), 404);
            }

            $classification->update($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Classificação atualizada com sucesso!', 200), 400);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function remove($id)
    {
        try {

            $classification     = $this->classification->find($id);

            if (! $classification) {
                return response()->json(\App\API\ApiError::errorMessage('Classificação não encontrado!', 404), 404);
            }

            $classification->delete();

            return response()->json(['data' => ['message' => 'Classificação: ' . $classification->name . ' removida com sucesso!']], 200);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500));
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }
}
