<?php

namespace App\Http\Controllers\Api;

use App\Actor;
use App\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActorController extends Controller
{
    private $actor;

    public function __construct(Actor $actor)
    {
        $this->actor = $actor;
    }

    public function index()
    {
        $data = ['data' => $this->actor->all()];
        return response()->json($data);
    }

    public function show($id)
    {
        $actor = $this->actor->find($id);

        if (! $actor) {
            return response()->json(\App\API\ApiError::errorMessage('Ator/Atriz não encontrado!', 404), 404);
        }

        $data = ['data' => $actor];
        return response()->json($data);
    }

    public function add(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_ator'      => 'required|string',
                    'data_nasc_ator' => 'required|date',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $this->actor->create($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Ator/Atriz criada com sucesso!', 201), 201);

        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function edit(Request $request, $id)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'nome_ator'      => 'required|string',
                    'data_nasc_ator' => 'required|date',
                ]);

            if ($validator->fails())
            {
                return response()
                    ->json($validator->errors());
            }

            $actor     = $this->actor->find($id);

            if (! $actor) {
                return response()->json(\App\API\ApiError::errorMessage('Ator/Atriz não encontrado!', 404), 404);
            }

            $actor->update($request->all());

            return response()->json(\App\API\ApiSuccess::successMessage('Ator/Atriz atualizada com sucesso!', 200), 400);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500), 500);
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }

    public function remove($id)
    {
        try {

            $actor     = $this->actor->find($id);

            if (! $actor) {
                return response()->json(\App\API\ApiError::errorMessage('Ator/Atriz não encontrado!', 404), 404);
            }

            $actor->delete();

            return response()->json(['data' => ['message' => 'Ator/Atriz: ' . $actor->name . ' removido com sucesso!']], 200);
        } catch (\Exception $e) {
            if (config('app.debug')) {
                return response()->json(\App\API\ApiError::errorMessage($e->getMessage(), 500));
            }
            return response()->json(\App\API\ApiError::errorMessage('A requisição possuí um formato inesperado.', 500));
        }
    }
}
