<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    protected $fillable = ['nome_ator', 'data_nasc_ator'];

    protected $dates = ['data_nasc_ator'];

    public function moviesActors()
    {
        return $this->hasMany('App\MoviesActors');
    }
}
