<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoviesActors extends Model
{
    protected $fillable = ['movies_id', 'actors_id'];

    function actor() {
        return $this->belongsTo('App\Actor');
    }

    function movie() {
        return $this->belongsTo('App\Movies');
    }
}
