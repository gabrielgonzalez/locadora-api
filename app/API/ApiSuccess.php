<?php

namespace App\API;

class ApiSuccess
{
    public static function successMessage($message, $code)
    {
        return [
            'data' => [
                'message' => $message,
                'code' => $code,
            ]
        ];
    }
}
