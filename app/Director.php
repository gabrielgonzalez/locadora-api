<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    protected $fillable = ['nome_diretor', 'data_nasc_diretor'];

    protected $dates = ['data_nasc_diretor'];

    public function movies()
    {
        return $this->hasMany('App\Movies');
    }
}
