<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_filme', 150);
            $table->string('ano_lancamento', 4);
            $table->longText('sinopse');
            $table->integer('classifications_id')->unsigned();
            $table->integer('directors_id')->unsigned();
            $table->foreign('classifications_id')
                ->references('id')
                ->on('classifications');
            $table->foreign('directors_id')
                ->references('id')
                ->on('directors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
